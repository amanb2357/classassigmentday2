package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class IntegerArray {

    public int sizee = 10;
    public int arr[];

    public IntegerArray(int size) {
        this.sizee = size;
        arr = new int[size];
    }

    public IntegerArray() {
        this.sizee = 10;
        arr = new int[sizee];
    }

    public IntegerArray(IntegerArray anotherArray)
    {
        arr = anotherArray.arr.clone();
        this.sizee = anotherArray.sizee;
    }

    public IntegerArray(int array[])
    {
        arr = array.clone();
        this.sizee = array.length;
    }

    public void display(){
        for(int i=0;i<sizee;i++)
            System.out.println(arr[i]);
    }

    public int sum(){
        int sum = 0;
        for(int i=0;i<sizee;i++)
            sum+=arr[i];
        return sum;
    }

    public int search(int n){
        for(int i=0;i<sizee;i++)
            if(arr[i]==n)
                return i;
            return 0;
    }

    public void sort()
    {
        Arrays.sort(arr);
    }

    public void read() throws IOException {
        for(int i=0;i<sizee;i++)
            arr[i]=Integer.parseInt((new BufferedReader(new InputStreamReader(System.in)).readLine()));
    }

}
